package lambda.anonymous;

import java.util.function.Consumer;

public class MyClassAppl {
	
	String instanceValue = "instanceValue1";
	static String staticValue = "staticValue1";
	
	public MyClassAppl(String instanceValue) {
		this.instanceValue = instanceValue;
	}
	
	public static void main(String[] args) {
		MyClassAppl myClassAppl = new MyClassAppl("instanceValue1");
		myClassAppl.start();
		
//		start2(10);
	}

	private static void start2(int i) {
		System.out.println(i);
	}

	private void start() {
		// ��������� ������-���������
		Consumer<Integer> consumer = (x) -> System.out.println(instanceValue);
		consumer.accept(10);
		
		// ������� ������-���������
		Consumer<Integer> consumer2 = (x) -> {
			
			instanceValue = "Hello";
			System.out.println(instanceValue);
			start2(10);
			System.out.println(this.instanceValue);
			
		};
		consumer2.accept(10);
		
		// ����� ��������� �����
		Consumer<Integer> consumer3 = new Consumer<Integer>() {
			@Override
			public void accept(Integer t) {
				String instanceValue = "Hi";
				System.out.println(instanceValue);
				start2(10);
				// this ���������� ������
//				this.
				System.out.println(MyClassAppl.this.instanceValue);
			}
		};
		consumer3.accept(10);
		
		System.out.println(instanceValue);
		
	}

}
