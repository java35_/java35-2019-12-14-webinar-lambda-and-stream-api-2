package lambda.anonymous;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class MyClassAppl2 {
	
	String instanceValue = "instanceValue1";
	static String staticValue = "staticValue1";
	

	public static void main(String[] args) {
		MyClassAppl2 myClassAppl = new MyClassAppl2();
//		myClassAppl.start();
//		
		myClassAppl.start2();
	}


//	int counter = 0;
	private void start2() {
		List<Integer> array = new ArrayList<>(Arrays.asList(5, 6, 7, 9));
		
		int[] counter = new int[1];
		
		int sum = array.stream()
						.mapToInt(x -> x)
						.map(x -> {
							if (x % 3 == 0)
								counter[0]++;
							return x;
						})
						.sum();
		
		System.out.println("Sum: " + sum);
		System.out.println("Counter: " + counter[0]);
	}


	private void start() {
		String localVariable = "localVariable1";
		Consumer<Integer> consumer = (x) -> System.out.println(localVariable);
		consumer.accept(10);
		
		Consumer<Integer> consumer2 = (x) -> {
//			localVariable = "Hello";
			System.out.println(localVariable);
		};
		consumer2.accept(10);
		
		// ����� ��������� �����
		Consumer<Integer> consumer3 = new Consumer<Integer>() {
			@Override
			public void accept(Integer t) {
				System.out.println(localVariable);
			}
		};
		consumer3.accept(10);
		
//		localVariable = "Hello";
		
		System.out.println(localVariable);
		
	}

}
