package lambda.exceptions;

public class Appl {

	public static void main(String[] args) throws Exception {
		
		IMyFunc iMyFunc = (v) -> {
			throw new Exception();
		};
		iMyFunc.accept(10);
		
		IMyFunc iMyFunc2 = new IMyFunc() {
			
			@Override
			public void accept(int value) throws Exception {
				throw new Exception();
			}
		};
	}
}