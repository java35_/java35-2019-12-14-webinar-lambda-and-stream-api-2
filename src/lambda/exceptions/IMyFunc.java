package lambda.exceptions;

public interface IMyFunc {
	void accept(int value) throws Exception;
}
