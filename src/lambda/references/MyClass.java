package lambda.references;

public class MyClass {
	static void staticMethod(int i) {
		System.out.println(i);
	}
	
	void instanceMethod(int i) {
		System.out.println(i);
	}
}
