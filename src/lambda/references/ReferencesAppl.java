package lambda.references;

import java.util.function.Consumer;

public class ReferencesAppl {

	public static void main(String[] args) {
//		Consumer<Integer> consumer = (x) -> MyClass.staticMethod(x);
//		Consumer<Integer> consumer = MyClass::staticMethod;
		
//		Consumer<Integer> consumer = Math::sqrt;
		
		MyClass myClass = new MyClass();
		Consumer<Integer> consumer = myClass::instanceMethod;
		
//		Integer[]::new;
	}

}