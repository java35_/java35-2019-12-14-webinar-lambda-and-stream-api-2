package sportloto;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SportLotoAppl {
	public static void main(String[] args) {
		Random rand = new Random();
		
		rand.ints(1, 49)
			.distinct()
			.limit(7)
			.forEach(x -> System.out.print(x + " "));
		
		System.out.println();
	
		List<List<Integer>> lists = new ArrayList<>();
		
//		List<Integer> list1 = new ArrayList<>(Arrays.asList(rand.nextInt(), 12, 13));
//		List<Integer> list2 = new ArrayList<>(Arrays.asList(21, 22, 23));
//		List<Integer> list3 = new ArrayList<>(Arrays.asList(31, 32, 33));
		
		
		List<Integer> list3 = rand.ints(1, 49)
									.distinct()
									.boxed()
									.limit(3)
									.collect(Collectors.toList());
		lists = Stream.generate(
							() -> rand.ints(1, 49)
									.distinct()
									.boxed()
									.limit(3)
									.collect(Collectors.toList()))
						.limit(3)
						.collect(Collectors.toList());
		lists.forEach(x -> {
			System.out.println();
			x.forEach(y -> System.out.print(y + " "));
		});
		
		lists.stream()
				.flatMap(x -> x.stream())
				.mapToInt(x -> x)
				.sum();
				
//		lists.add(list1);
//		lists.add(list2);
//		lists.add(list3);
		
	}
}