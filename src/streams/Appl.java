package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Appl {
	public static void main(String[] args) {
//		MyFuncClass myFuncClass = MyFuncClass.getInstance();
		
		MyFuncClass.of("Hello", "Hi")
					.add("Shalom")
					.add("...")
					.printValues()
					.add("Privet")
//					.filter(new Predicate<String>() {
//						@Override
//						public boolean test(String t) {
//							return t.length() < 4;
//						}
//					})
					.filter((t) -> t.length() < 4)
					.map(v -> v + "123")
					.printValues();
					
//					.filter()
//					.map()
//					.forEach();
		
//		Stream.of("sdsd", "sdfsds").map(mapper)
	}
}
