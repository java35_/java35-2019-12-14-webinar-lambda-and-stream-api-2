package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class MyFuncClass {
	private List<String> list;
	
	static MyFuncClass getInstance() {
		return new MyFuncClass();
	}
	
	static public MyFuncClass of(String ...array) {
		MyFuncClass myFuncClass = new MyFuncClass(); 
		myFuncClass.list = new ArrayList<>(Arrays.asList(array));
		return myFuncClass;
	}
	
	public MyFuncClass add(String str) {
		list.add(str);
		return this;
	}
	
	public MyFuncClass printValues() {
		for (String string : list) {
			System.out.println(string);
		}
		return this;
	}
	
	public MyFuncClass remove(String str) {
		list.remove(str);
		return this;
	}
	
	public MyFuncClass filter(Predicate<String> predicate) {
		Iterator<String> itr = list.iterator();
		while (itr.hasNext()) {
			if (predicate.test(itr.next()))
				itr.remove();
		}
		return this;
	}
	
	public MyFuncClass map(Function<String, String> mapper) {
		for (int i = 0; i < list.size(); i++) {
			list.set(i, mapper.apply(list.get(i)));
		}
		return this;
	}
	
}
